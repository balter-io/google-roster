#!/usr/bin/env python3

import sys
import xlrd
from datetime import datetime, timedelta
import shift_template

roster = sys.argv[1]

if len(sys.argv) < 1:
    print('Please specify the location of the roster spreadsheet')

workbook = xlrd.open_workbook(roster)
first_sheet = workbook.sheet_by_index(0)

shift_code = ['ZW', 'LM6', 'EE', 'E', 'LD', 'E6', 'LM', 'RL', 'EK', 'NA', 'ZW8', 'NU']


def obtain_start_date():

    is_valid = False
    while not is_valid:
        user_in = input("What is the roster start date? (yyyy-mm-dd): ").strip()
        try:    # strptime throws an exception if the input doesn't match the pattern
            start = datetime.strptime(user_in, "%Y-%m-%d").date()
            is_valid = True
        except SyntaxError:
            print("Doh, invalid date format! (yyyy-mm-dd)\n")
    return start


for sheet in workbook.sheets():
    for rowid in range(sheet.nrows):
        row = sheet.row(rowid)
        for colid, cell in enumerate(row):
            if cell.value == 'INSERT NAME HERE':    # EXACTLY as its written on the roster (between the single quotes).
                print()

                row = rowid
                column = colid

                full_row = sheet.row_values(row)
                # print(full_row)

                name = full_row[0]
                # print(name)

                staff_id = int(full_row[1])
                # print(staff_id)

                shifts = full_row[4:32]
                # print(f'The name location is Row:{rowid} Column:{colid}')

                # print(shifts)
                # print(days)

                start_date = obtain_start_date()
                new_events = []

                for shift, date in enumerate(shifts):
                    shift_day = (start_date + timedelta(days=shift)).strftime('%Y-%m-%d')
                    # print(date + ' on ' + shift_day)

                    roster_method = getattr(shift_template.Shift, date.lower())
                    result = roster_method(shift_day)
                    new_events.append(result)
                # print(new_events)
