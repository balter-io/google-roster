# Google Roster/Calendar

![Imgur](https://i.imgur.com/66Jlujg.png)

### Table of Contents

-   [Description](#Description)
-   [Installation](#Installation)
-   [Usage](#Usage)
-   [Learning Outcomes](#Learning-Outcomes)
-   [Road Map](#Road-Map)
---
### Description

This program parses the RBWH Emergency Nurse rosters via an excel spreadsheet and creates objects that integrate with
the Google Calendar API. It easily uploads your roster to the calendar including shift summary, date, start/end times
and event location.

---

### Installation
This program has only been used on a Linux platform, use at your own risk elsewhere. You will need Python 3 installed 
on your machine, then:
-   Run `pip install -r requirements.txt`

---

### Usage
The program can only be used on linux based systems from a terminal at present. It takes one command line argument and
that must be in the form of the path to the location of the excel roster file.
 
To run the program:

1. CD into the location you wish to save this repo
2. Clone this repo
3. Open `roster_parser.py` and follow the instructions on `line 36` - Save the file
4. Open `google_api.py` and insert your calendar id onto `line 44` - Save the file
    NB: The calendar ID can be found in the settings from your calendar
5. Open a terminal
6. CD into the directory housing the google_api.py file
7. Run: `python google_api.py <insert_roster_location_path_here>`
8. A prompt will ask you for the start date of your roster period (from the excel file). The format MUST be yyyy-mm-dd

A summary of the inserted events will be printed to the screen.

---

### Learning Outcomes

-   Interaction with the Google Calendar API
-   Iteration over a `.xlsx` file using the XLRD Python package
-   Time formatting ('strptime' and 'Timedelta')
-   Pickling
-   Command line arguments for user interaction
-   Object interaction
-   Creation of a `requirements.txt` file
-   Markdown

---

### Road Map
-   End User GUI
-   User Database

---
[Back to the top](#Back-to-the-top)
