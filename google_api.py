#!/usr/bin/env python3

import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import roster_parser

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']


def main():

    creds = None

    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.

    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    calendar_api_creds = os.environ.get('calendar_api_creds')

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(calendar_api_creds, SCOPES)
            creds = flow.run_local_server()

        # Save the credentials for the next run

        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)

    for new_event in roster_parser.new_events:
        new_event = service.events().insert(calendarId=os.environ.get('calendar_id'),   # calendar ID goes here
                                            body=new_event, sendNotifications=None,
                                            sendUpdates=None).execute()

        print(new_event)


if __name__ == '__main__':
    main()
