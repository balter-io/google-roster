from datetime import datetime, timedelta


class Shift:

    @staticmethod
    def e(date):  # early shift one (E): 07:15-15:45
        return {
            'summary': 'Work (E)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T07:15:00',
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T15:45:00'
            },
        }

    @staticmethod
    def e6(date):   # early shift two (E6): 07:15-13:45
        return {
            'summary': 'Work (E6)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T07:15:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T13:45:00'
            }
        }

    @staticmethod
    def ek(date):   # morning shift (EK): 10:00-18:30
        return {
            'summary': 'Work (EK)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T10:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T18:30:00'
            }
        }

    @staticmethod
    def ld(date):  # afternoon shift one (LD): 12:45-21:15
        return {
            'summary': 'Work (LD)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T12:45:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T21:15:00'
            }
        }

    @staticmethod
    def lm(date):   # afternoon shift two (LM): 14:45-23:15
        return {
            'summary': 'Work (LM)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T14:45:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T23:15:00'
            }
        }

    @staticmethod
    def lm6(date):  # evening shift (LM6): 14:45-21:15
        return {
            'summary': 'Work (LM6)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T14:45:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T21:15:00'
            }
        }

    @staticmethod
    def na(date):   # disco shift (NA): 18:00-02:30
        end_date = datetime.date(date) + timedelta(days=1)

        return {
            'summary': 'Work (NA)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T18:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f"{end_date.strftime('%Y-%m-%d')}T02:30:00"
            }
        }

    @staticmethod
    def zw(date):  # night shift one (ZW): 21:00-0730
        end_date = datetime.strptime(date, "%Y-%m-%d") + timedelta(days=1)

        return {
            'summary': 'Work (ZW)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T21:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f"{end_date.strftime('%Y-%m-%d')}T07:30:00"
            }
        }

    @staticmethod
    def zw8(date):  # night shift two (ZW8): 21:00-05:30
        end_date = datetime.strptime(date, "%Y-%m-%d") + timedelta(days=1)

        return {
            'summary': 'Work (ZW8)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T21:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f"{end_date.strftime('%Y-%m-%d')}T05:30:00"
            }
        }

    @staticmethod
    def nu(date):   # night shift three (NU): 23:00-07:30
        end_date = datetime.strptime(date, "%Y-%m-%d") + timedelta(days=1)

        return {
            'summary': 'Work (NU)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T23:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f"{end_date.strftime('%Y-%m-%d')}T07:30:00"
            }
        }

    @staticmethod
    def ee(date):  # offline duty (EE): 08:00-16:30
        return {
            'summary': 'Work (EE)',
            'location': "Royal Brisbane and Women's Hospital",
            'start': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T08:00:00'
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'dateTime': f'{date}T16:30:00'
            }
        }

    @staticmethod
    def rl(date):   # rec leave (RL): 00:00-23:59
        return {
            'summary': 'Rec Leave',
            'location': 'Home',
            'start': {
                'timeZone': 'Australia/Brisbane',
                'date': f'{date}'   # T00:00:00
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'date': f'{date}'   # T23:59:00
            }
        }

    @staticmethod
    def do(date):    # day off (DO): 00:00-23:59
        return {
            'summary': 'Day Off',
            'location': 'Home',
            'start': {
                'timeZone': 'Australia/Brisbane',
                'date': f'{date}'   # T00:00:00
            },
            'end': {
                'timeZone': 'Australia/Brisbane',
                'date': f'{date}'   # T23:59:00
            }
        }
